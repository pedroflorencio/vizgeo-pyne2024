# VizGeo

Repositório de materiais do tutorial **Visualização e Processamento de Dados Geoespaciais: Uma Introdução Prática**, apresentado no evento Python Nordeste 2024.

**Autor:** Pedro Florencio | Cientista de Dados @Iplanfor

Notebook: https://colab.research.google.com/drive/1mMg48BDXcXogcqjKBdPyMqKNlDeybl_L